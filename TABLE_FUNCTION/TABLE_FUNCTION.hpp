//
//  function_with_memory.hpp
//  function_with_memory
//
//  Created by Артём on 19/10/2018.
//  Copyright © 2018 Ерошев Артём. All rights reserved.
//

#ifndef function_with_memory_
#define function_with_memory_

/* The classes below are exported */
#pragma GCC visibility push(default)
#include <iostream>

namespace function{
    struct Point{
        double x;
        double y;
        Point(double x0 = 0, double y0 = 0) :x(x0), y(y0){}
    };
    
    class ifc{
    private:
        static const int QUOTA = 20;
        int SIZE;
        int occup;
        Point *nodes;
    public:
        //ALL METHODS ACTUAL FOR SORT ARRAY!!!!!!
        //Construct
        ifc(): SIZE(0), occup(0), nodes(nullptr){std::cout << "Work standart constructor" << std::endl;}
        ifc(double x0, double y0);
        ifc(const Point &p0);
        ifc(Point *object, int SZ);
        //
        ifc(const ifc &);
        ifc(ifc &&);
        //
        ~ifc(){ std::cout << "Work deconstructor" << std::endl; delete [] nodes; }
        ifc &operator =(const ifc&);
        ifc &operator =(ifc &&);
        //Set
        ifc & Push_Node(Point &p);
        ifc & Push_Node(double x, double y);
        ifc & Insertion_sort();
        ifc & operator += (const ifc &object);
        
        std::istream & stream_input(std::istream &flow);
        friend std::istream & operator >> (std::istream &flow, ifc &object);// to do
        //Get
        friend std::ostream & operator << (std::ostream &flow, const ifc &object);
        std::ostream & print(std::ostream &flow) const;
        double min_function() const;
        double max_function() const;
        int define_function() const;
        operator int() const;
        double count_in_dot(double x0) const;
        double operator () (double x0) const;
        
        const Point ACESS_NODES(int i) const {return nodes[i];}
        const int ACESS_OCCUP() const {return occup;}
        const int ACESS_SIZE() const {return SIZE;}
        const Point* ACESS_PTR() const {return nodes;}
    };
};
#pragma GCC visibility pop
#endif
