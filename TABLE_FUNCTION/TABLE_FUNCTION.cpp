//
//  function_with_memory.cpp
//  
//
//  Created by Артём on 19/10/2018.
//  Copyright © 2018 Ерошев Артём. All rights reserved.
//

#include <iostream>
#include "TABLE_FUNCTION.hpp"
#include "Library_Template.hpp"

namespace function {
    ifc::ifc(double x0, double y0): SIZE(1), occup(0), nodes(new Point[1]){
        nodes[occup].x = x0;
        nodes[occup++].y = y0;
        std::cout << "Work constructor with one point" << std::endl;
    }
    
    ifc::ifc(const Point &p0): SIZE(1), occup(0),nodes(new Point[1]){
        nodes[occup++] = p0;
        std::cout << "Work constructor with one point" << std::endl;
    }
    
    ifc::ifc(Point *object, int SZ): SIZE(SZ), occup(0), nodes(new Point[SZ]){
        std::cout << "Work constructor with array" << std::endl;
        if (object == nullptr)
        {
            delete [] nodes;
            throw std::bad_alloc();
        }
        
        bool flag = true;
        for (int i = 0; i < SZ; i++){
            for (int j = occup - 1; j >= 0; j--){
                if (object[i].x == nodes[j].x){
                    nodes[j].y = (nodes[j].y + object[i].y)/2;
                    flag = false;
                    break;
                }
            }
            if (flag){
                nodes[i] = object[i];
                occup++;
                flag = true;
            }
        }
        this->Insertion_sort();
        if (SIZE != occup){
            SIZE = occup;
            nodes = ltmp::renew(nodes, SIZE);
        }
    }//to do
    
    std::ostream & ifc::print(std::ostream &flow) const{
        if (occup <= 0)
            flow << "Nodes is empty";
        else
            for (int i = 0; i < occup; i++)
                flow << "(" << nodes[i].x << "," << nodes[i].y << ") ";
        flow << std::endl;
        return flow;
    }
    
    double ifc::min_function() const{
        if (occup == 0)
            throw std::out_of_range("Nodes is empty");
        
        double min = nodes[0].y;
        for (int i = 1; i < occup; i++){
            if (nodes[i].y < min)
                min = nodes[i].y;
        }
        return min;
    }
    
    double ifc::max_function() const{
        if (occup == 0)
            throw std::out_of_range("Nodes is empty");
        
        double max = nodes[0].y;
        for (int i = 1; i < occup; i++){
            if (nodes[i].y > max)
                max = nodes[i].y;
        }
        return max;
    }
    
    double ifc::count_in_dot(double x0) const{
        if (occup <= 1)
            throw std::out_of_range("Impossible to count");
        if (x0 > nodes[occup - 1].x)
        {
            double k = (nodes[occup - 1].y - nodes[occup - 2].y)/(nodes[occup - 1].x - nodes[occup - 2].x);
            double b = nodes[occup - 2].y - (k * nodes[occup - 2].x);
            return (k * x0) + b;
        }
        else if (x0 < nodes[0].x)
        {
            double k = (nodes[1].y - nodes[0].y)/(nodes[1].x - nodes[0].x);
            double b = nodes[0].y - (k * nodes[0].x);
            return (k * x0) + b;
        }
        {
            int left = 0;
            int right = occup - 1;
            int midd = 0;
            while(1){
                midd = (left + right)/2;
                
                if (x0 < nodes[midd].x)
                    if (x0 > nodes[midd - 1].x)
                    {
                        double k = (nodes[midd].y - nodes[midd - 1].y)/(nodes[midd].x - nodes[midd - 1].x);
                        double b = nodes[midd - 1].y - (k * nodes[midd - 1].x);
                        return (k * x0) + b;
                    }
                    else
                        right = midd - 1;
                    else if (x0 > nodes[midd].x)
                        if (x0 < nodes[midd + 1].x)
                        {
                            double k = (nodes[midd + 1].y - nodes[midd].y)/(nodes[midd + 1].x - nodes[midd].x);
                            double b = nodes[midd].y - (k * nodes[midd].x);
                            return (k * x0) + b;
                        }
                        else
                            left = midd + 1;
                        else
                            return nodes[midd].y;
            }
        }
    }
    
    int ifc::define_function() const{
        if (occup <= 1)
            throw std::invalid_argument("Impossible define type function");
        
        double k = 0;
        double last_no_zero = 0;
        
        for (int i = 1; i < occup; i++){
            k = (nodes[i].y - nodes[i - 1].y)/(nodes[i].x - nodes[i - 1].x);
            if (last_no_zero == 0 && k != 0)
                last_no_zero = k;
            if (last_no_zero < 0 && k > 0)
                return 2;
            if (last_no_zero > 0 && k < 0)
                return 2;
        }
        if (last_no_zero > 0)
            return 1;
        else if (last_no_zero < 0)
            return -1;
        else
            return 0;
    }
    
    ifc & ifc::Push_Node(double x,double y){
        if (occup >= SIZE){
            SIZE += 1;
            nodes = ltmp::renew(nodes, SIZE);
        }
        
        if (x > nodes[occup - 1].x){
            nodes[occup].x = x;
            nodes[occup++].y = y;
            return *this;
        }
        
        if (x < nodes[0].x){
            for (int k = occup; k > 0; k--)
                nodes[k] = nodes[k - 1];
            nodes[0].x = x;
            nodes[0].y = y;
            occup++;
            return *this;
        }
        
        int left = 0;
        int right = occup - 1;
        int midd = 0;
        while(1){
            midd = (left + right)/2;
            
            if (x < nodes[midd].x)
                if (x  > nodes[midd - 1].x)
                {
                    for (int k = occup; k > midd; k--)
                        nodes[k] = nodes[k - 1];
                    nodes[midd].x = x;
                    nodes[midd].y = y;
                    occup++;
                    break;
                }
                else
                    right = midd - 1;
                else if (x  > nodes[midd].x)
                    if (x < nodes[midd + 1].x)
                    {
                        for (int k = occup; k > midd + 1; k--)
                            nodes[k] = nodes[k - 1];
                        nodes[midd + 1].x = x;
                        nodes[midd + 1].y = y;
                        occup++;
                        break;
                    }
                    else
                        left = midd + 1;
                    else{
                        nodes[midd].y = (nodes[midd].y + y)/2;
                        break;
                    }
        }
        if (SIZE != occup){
            SIZE = occup;
            nodes = ltmp::renew(nodes, SIZE);
        }
        return *this;
    }
    
    ifc & ifc::Push_Node(Point &p){
        if (occup >= SIZE){
            SIZE += 1;
            nodes = ltmp::renew(nodes, SIZE);
        }
        
        if (p.x > nodes[occup - 1].x){
            nodes[occup++] = p;
            return *this;
        }
        
        if (p.x < nodes[0].x){
            for (int k = occup; k > 0; k--)
                nodes[k] = nodes[k - 1];
            nodes[0] = p;
            occup++;
            return *this;
        }
        
        int left = 0;
        int right = occup - 1;
        int midd = 0;
        while(1){
            midd = (left + right)/2;
            
            if (p.x < nodes[midd].x)
                if (p.x  > nodes[midd - 1].x)
                {
                    for (int k = occup; k > midd; k--)
                        nodes[k] = nodes[k - 1];
                    nodes[midd] = p;
                    occup++;
                    break;
                }
                else
                    right = midd - 1;
                else if (p.x  > nodes[midd].x)
                    if (p.x < nodes[midd + 1].x)
                    {
                        for (int k = occup; k > midd + 1; k--)
                            nodes[k] = nodes[k - 1];
                        nodes[midd + 1] = p;
                        occup++;
                        break;
                    }
                    else
                        left = midd + 1;
                    else{
                        nodes[midd].y = (nodes[midd].y + p.y)/2;
                        break;
                    }
        }
        if (SIZE != occup){
            SIZE = occup;
            nodes = ltmp::renew(nodes, SIZE);
        }
        return *this;
    }
    
    std::istream & ifc::stream_input(std::istream &flow){
        std::cout << "Input value nodes" << std::endl;
        int SZ;
        ltmp::GET_NUM(SZ, SIZE - 1);
        
        delete [] nodes;
        SIZE = SZ;
        nodes = new Point(SIZE);
        occup = 0;
        
        Point p0;
        p0.x = 0; p0.y = 0;
        
        for (int counter = 0; counter < SZ; counter++)
        {
            std::cout << "Input x = ";
            ltmp::GET_NUM_FLOW(p0.x, flow);
            std::cout << "Input y = ";
            ltmp::GET_NUM_FLOW(p0.y, flow);
            
            if (p0.x > nodes[occup - 1].x){
                nodes[occup++] = p0;
                continue;
            }
            
            if (p0.x < nodes[0].x){
                for (int k = occup; k > 0; k--)
                    nodes[k] = nodes[k - 1];
                nodes[0] = p0;
                occup++;
                continue;
            }
            
            int left = 0;
            int right = occup - 1;
            int midd = 0;
            while(1){
                midd = (left + right)/2;
                
                if (p0.x < nodes[midd].x)
                    if (p0.x  > nodes[midd - 1].x)
                    {
                        for (int k = occup; k > midd; k--)
                            nodes[k] = nodes[k - 1];
                        nodes[midd] = p0;
                        occup++;
                        break;
                    }
                    else
                        right = midd - 1;
                    else if (p0.x  > nodes[midd].x)
                        if (p0.x < nodes[midd + 1].x)
                        {
                            for (int k = occup; k > midd + 1; k--)
                                nodes[k] = nodes[k - 1];
                            nodes[midd + 1] = p0;
                            occup++;
                            break;
                        }
                        else
                            left = midd + 1;
                        else{
                            nodes[midd].y = (nodes[midd].y + p0.y)/2;
                            break;
                        }
            }
        }
        if (SIZE > occup){
            SIZE = occup;
            nodes = ltmp::renew(nodes, SIZE);
        }
        return flow;
    }
    
    ifc & ifc::operator += (const ifc &object){
        if (occup + object.occup > SIZE){
            SIZE = occup + object.occup;
            nodes = ltmp::renew(nodes, SIZE);
        }
        
        for (int i = 0; i < object.occup; i++){
            
            if (object.nodes[i].x > nodes[occup - 1].x){
                nodes[occup++] = object.nodes[i];
                continue;
            }
            
            if (object.nodes[i].x < nodes[0].x){
                for (int k = occup; k > 0; k--)
                    nodes[k] = nodes[k - 1];
                nodes[0] = object.nodes[i];
                occup++;
                continue;
            }
            
            int left = 0;
            int right = occup - 1;
            int midd = 0;
            while(1){
                midd = (left + right)/2;
                
                if (object.nodes[i].x < nodes[midd].x)
                    if (object.nodes[i].x  > nodes[midd - 1].x)
                    {
                        for (int k = occup; k > midd; k--)
                            nodes[k] = nodes[k - 1];
                        nodes[midd] = object.nodes[i];
                        occup++;
                        break;
                    }
                    else
                        right = midd - 1;
                    else if (object.nodes[i].x  > nodes[midd].x)
                        if (object.nodes[i].x < nodes[midd + 1].x)
                        {
                            for (int k = occup; k > midd + 1; k--)
                                nodes[k] = nodes[k - 1];
                            nodes[midd + 1] = object.nodes[i];
                            occup++;
                            break;
                        }
                        else
                            left = midd + 1;
                        else{
                            nodes[midd].y = (nodes[midd].y + object.nodes[i].y)/2;
                            break;
                        }
            }
        }
        if (SIZE != occup){
            SIZE = occup;
            nodes = ltmp::renew(nodes, SIZE);
        }
        return *this;
    }
    
    ifc & ifc::Insertion_sort(){
        if (occup <= 0)
            throw std::out_of_range("Nodes is empty");
        
        Point tmp;
        for(int i = 1; i < occup; i++)
            for(int j = i;j > 0 && nodes[j-1].x > nodes[j].x; j--){
                ltmp::swap(nodes[j - 1], nodes[j]);
            }
        
        return *this;
    }
    
    std::ostream & operator << (std::ostream &flow, const ifc &object) {
        if (object.occup <= 0)
            flow << "Nodes is empty";
        else
            for (int i = 0; i < object.occup; i++)
                flow << "(" << object.nodes[i].x << "," << object.nodes[i].y << ") ";
        return flow;
    }
    
    std::istream & operator >> (std::istream &flow, ifc &object){
        std::cout << "Input value nodes" << std::endl;
        int SZ;
        ltmp::GET_NUM(SZ, object.SIZE - 1);
        
        delete [] object.nodes;
        object.SIZE = SZ;
        object.nodes = new Point(object.SIZE);
        object.occup = 0;
        
        Point p0;
        p0.x = 0; p0.y = 0;
        
        for (int counter = 0; counter < SZ; counter++)
        {
            std::cout << "Input x = ";
            ltmp::GET_NUM_FLOW(p0.x, flow);
            std::cout << "Input y = ";
            ltmp::GET_NUM_FLOW(p0.y, flow);
            
            if (p0.x > object.nodes[object.occup - 1].x){
                object.nodes[object.occup++] = p0;
                continue;
            }
            
            if (p0.x < object.nodes[0].x){
                for (int k = object.occup; k > 0; k--)
                    object.nodes[k] = object.nodes[k - 1];
                object.nodes[0] = p0;
                object.occup++;
                continue;
            }
            
            int left = 0;
            int right = object.occup - 1;
            int midd = 0;
            while(1){
                midd = (left + right)/2;
                
                if (p0.x < object.nodes[midd].x)
                    if (p0.x  > object.nodes[midd - 1].x)
                    {
                        for (int k = object.occup; k > midd; k--)
                            object.nodes[k] = object.nodes[k - 1];
                        object.nodes[midd] = p0;
                        object.occup++;
                        break;
                    }
                    else
                        right = midd - 1;
                    else if (p0.x  > object.nodes[midd].x)
                        if (p0.x < object.nodes[midd + 1].x)
                        {
                            for (int k = object.occup; k > midd + 1; k--)
                                object.nodes[k] = object.nodes[k - 1];
                            object.nodes[midd + 1] = p0;
                            object.occup++;
                            break;
                        }
                        else
                            left = midd + 1;
                        else{
                            object.nodes[midd].y = (object.nodes[midd].y + p0.y)/2;
                            break;
                        }
            }
        }
        if (object.SIZE != object.occup){
            object.SIZE = object.occup;
            object.nodes = ltmp::renew(object.nodes, object.SIZE);
        }
        return flow;
    }
    
    ifc::operator int() const{
        if (occup <= 1)
            throw std::invalid_argument("Impossible define type function");
        
        double k = 0;
        double last_no_zero = 0;
        
        for (int i = 1; i < occup; i++){
            k = (nodes[i].y - nodes[i - 1].y)/(nodes[i].x - nodes[i - 1].x);
            if (last_no_zero == 0 && k != 0)
                last_no_zero = k;
                if (last_no_zero < 0 && k > 0)
                    return 2;
            if (last_no_zero > 0 && k < 0)
                return 2;
        }
        if (last_no_zero > 0)
            return 1;
        else if (last_no_zero < 0)
            return -1;
        else
            return 0;
    }
    
    double  ifc::operator () (double x0) const{
        if (occup <= 1)
            throw std::out_of_range("Impossible to count");
        if (x0 > nodes[occup - 1].x)
        {
            double k = (nodes[occup - 1].y - nodes[occup - 2].y)/(nodes[occup - 1].x - nodes[occup - 2].x);
            double b = nodes[occup - 2].y - (k * nodes[occup - 2].x);
            return (k * x0) + b;;
        }
        else if (x0 < nodes[0].x)
        {
            double k = (nodes[1].y - nodes[0].y)/(nodes[1].x - nodes[0].x);
            double b = nodes[0].y - (k * nodes[0].x);
            return (k * x0) + b;
        }
        {
            int left = 0;
            int right = occup - 1;
            int midd = 0;
            while(1){
                midd = (left + right)/2;
                
                if (x0 < nodes[midd].x)
                    if (x0 > nodes[midd - 1].x)
                    {
                        double k = (nodes[midd].y - nodes[midd - 1].y)/(nodes[midd].x - nodes[midd - 1].x);
                        double b = nodes[midd - 1].y - (k * nodes[midd - 1].x);
                        return (k * x0) + b;
                    }
                    else
                        right = midd - 1;
                    else if (x0 > nodes[midd].x)
                        if (x0 < nodes[midd + 1].x)
                        {
                            double k = (nodes[midd + 1].y - nodes[midd].y)/(nodes[midd + 1].x - nodes[midd].x);
                            double b = nodes[midd].y - (k * nodes[midd].x);
                            return (k * x0) + b;
                        }
                        else
                            left = midd + 1;
                        else
                            return nodes[midd].y;
            }
        }
    }
    
    ifc::ifc(const ifc &object): SIZE(object.SIZE), occup(object.occup){
        //std::cout << "Work copy constructor" << std::endl; // for testing
        nodes = new Point[SIZE];
        for(int i = 0; i < occup; i++){
            nodes[i].x = object.nodes[i].x;
            nodes[i].y = object.nodes[i].y;
        }
    }
    
    ifc::ifc(ifc &&object): SIZE(object.SIZE), occup(object.occup), nodes(object.nodes){
        //std::cout << "Work move constructor" << std::endl; // for testing
        object.nodes = nullptr;
    }
    
    ifc & ifc::operator = (const ifc &object){
        //std::cout << "Work copy constructor" << std::endl; // for testing
        if (this != &object){
            SIZE = object.SIZE;
            occup = object.occup;
            delete [] nodes;
            nodes = new Point[SIZE];
            for (int i = 0; i < occup; i++){
                nodes[i].x = object.nodes[i].x;
                nodes[i].y = object.nodes[i].y;
            }
        }
        return *this;
    }
    
    ifc & ifc::operator = (ifc &&object){
        //std::cout << "Work move constructor" << std::endl; // for testing
        ltmp::swap(occup, object.occup);
        ltmp::swap(SIZE, object.SIZE);
        ltmp::swap(nodes, object.nodes);
        
        return *this;
    }
    
    //Emergency search for count in dot
    /*
     double result = 0;
     for (int i = 1; i < occup; i++)
     {
     //std::cout << i << std::endl;
     if (nodes[i - 1].x < x0 && x0 < nodes[i].x){
     double k = (nodes[i].y - nodes[i - 1].y)/(nodes[i].x - nodes[i - 1].x);
     double b = nodes[i - 1].y - (k * nodes[i - 1].x);
     result = ((k * x0) + b);
     return result;
     }
     if (nodes[i - 1].x == x0)
     {
     result = nodes[i - 1].y;
     return result;
     }
     if (nodes[i].x == x0){
     result = nodes[i].y;
     return result;
     }
     
     }
     */
    //Emergency search for +=
    /*for(int j = occup - 1; j >= 0; j--){
     
     if (object.nodes[i].x == nodes[j].x){
     nodes[j].y = (nodes[j].y + object.nodes[i].y)/2;
     break;
     }
     if (object.nodes[i].x > nodes[j].x && object.nodes[i].x < nodes[j + 1].x)
     {
     for (int k = occup; k > j; k--)
     nodes[k] = nodes[k - 1];
     nodes[j + 1] = object.nodes[i];
     occup++;
     break;
     }
     }*/
    //Emergency bad search for Push_node
    /*bool flag = true;
     for(int j = occup - 1; j >= 0; j--){
     if (p.x == nodes[j].x){
     nodes[j].y = (nodes[j].y + p.y)/2;
     flag = false;
     break;
     }
     }
     if (flag)
     nodes[occup++] = p;
     
     if (nodes[occup - 1].x < nodes[occup - 2].x)
     this->Insertion_sort();*/
    //Emergency bad search >>
    /*double x = 0, y = 0;
     int j = 0;
     bool flag = false;
     for (int counter = 0; counter < SZ; counter++)
     {
     std::cout << "Input x = ";
     ltmp::GET_NUM_FLOW(x, flow);
     for (j = object.occup - 1; j >= 0; j--)
     if (x == object.nodes[j].x)
     {
     
     flag = true;
     break;
     }
     std::cout << "Input y = ";
     ltmp::GET_NUM_FLOW(y, flow);
     if (flag){
     object.nodes[j].y = (object.nodes[j].y + y)/2;
     flag = false;
     }
     else{
     object.nodes[object.occup].y = y;
     object.nodes[object.occup++].x = x;
     }
     }
     if (object.occup > 0)
     object.Insertion_sort();*/
};


