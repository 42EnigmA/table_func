#Table Function
*  *  *
Welcome in my first open class. Thank you for choosing me. 
The repo has 2 different components. All .xcodeproject files are usefull only on Mac OS with Xcode.

This class calculate point according to principle piecewise linear interpolation.
For correct working necesary plug my library templates to class table_function.

LINK FOR FEEDBACK [42EnigmA](https://vk.com/aeroshev)

#Idea
*  *  *

This is methods use just for test class.
~~~~
const Point ACESS_NODES(int i) const {return nodes[i];}
const int ACESS_OCCUP() const {return occup;}
const int ACESS_SIZE() const {return SIZE;}
const Point* ACESS_PTR() const {return nodes;}
~~~~

#Constructor and desctructor
*  *  *
I just keep piece of my code
~~~~
ifc(): SIZE(0), occup(0), nodes(nullptr){std::cout << "Work standart constructor" << std::endl;}
ifc(double x0, double y0);
ifc(const Point &p0);
ifc(Point *object, int SZ);
~ifc(){ std::cout << "Work deconstructor" << std::endl; delete [] nodes; }
~~~~

#Methods
*  *  *
1.  += - add new point or array of point
2.  object(dot) - calculating value function in point x
3.  int(object) - define type function, this function retrurn: 
    -1 - if function down
     0 - if function constant
     1 - if function up
     2 - if fucntion variable
4.  "<<" - output in stream values array
5.  ">>" - rewrite data in array
6.  max_function - find max value function
7.  min_function - find min value function



