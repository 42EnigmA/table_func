//
//  Library_Template.hpp

//
//  Created by Артём on 10/10/2018.
//  Copyright © 2018 Ерошев Артём. All rights reserved.
//

#ifndef Library_Template_
#define Library_Template_

/* The classes below are exported */
#pragma GCC visibility push(default)

#include <iostream>

namespace ltmp {
    //return number in limit 0...size
    template <class NUMBER>
    int GET_NUM(NUMBER &num, NUMBER size){
        std::cin >> num;
        while (!(num >= 0 && num <= size)  || (!std::cin.good()))
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits <std::streamsize>::max(), '\n');
            std::cout << "Error" << std::endl;
            std::cin >> num;
        }
        return 1;
    }
    
    //return good number
    template <class NUMBER>
    int GET_NUM(NUMBER &num){
        std::cin >> num;
        while (!std::cin.good())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits <std::streamsize>::max(), '\n');
            std::cout << "Error" << std::endl;
            std::cin >> num;
        }
        return 1;
    }
    
    // return number > 0
    template <class NUMBER>
    int GET_NUM_SIZE(NUMBER &num){
        std::cin >> num;
        while (!(num > 0) || !std::cin.good())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits <std::streamsize>::max(), '\n');
            std::cout << "Error" << std::endl;
            std::cin >> num;
        }
        return 1;
    }
    
    template <class NUMBER, class STREAM>
    int GET_NUM_FLOW(NUMBER &num, STREAM &flow){
        flow >> num;
        while (!flow.good())
        {
            flow.clear();
            flow.ignore(std::numeric_limits <std::streamsize>::max(), '\n');
            std::cout << "Error" << std::endl;
            flow >> num;
        }
        return 1;
    }
    
    template <class NUMBER>
    int GET_NUM_CHOICE(NUMBER &num, NUMBER size){
        std::cin >> num;
        while (!(num > 0 && num <= size)  || (!std::cin.good()))
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits <std::streamsize>::max(), '\n');
            std::cout << "Error" << std::endl;
            std::cin >> num;
        }
        return 1;
    }
    
    template <class NUMBER>
    void swap(NUMBER &x, NUMBER &y){
        NUMBER tmp = x;
        x = y;
        y = tmp;
    }
    template <class POINTER>
    POINTER *renew(POINTER *ptr, unsigned int SIZE){
        if (SIZE == 0)
           return ptr;
        unsigned int SZ = sizeof(ptr)/sizeof(POINTER);
        if (SIZE == SZ)
            return ptr;
        POINTER *old = ptr;
        ptr = new POINTER[SIZE];
        unsigned int limit  = 0;
        if (SZ > SIZE)
            limit = SIZE;
        else
            limit = SZ;
        for (int i = 0; i < limit; i++)
            ptr[i] = old[i];
        delete [] old;
        return ptr;
    }
}

#pragma GCC visibility pop
#endif
